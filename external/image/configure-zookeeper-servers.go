package main

import "fmt"
import "log"
import "os"
import "bytes"
import "strconv"
import "io/ioutil"
import "path/filepath"
import "encoding/base64"
import "gopkg.in/yaml.v2"

type config struct {
	SupervisorSlotPorts []int    `supervisor.slots.ports,omitempty`
	ZkServers           []string `storm.zookeeper.servers`
	ZkRoot              string   `storm.zookeeper.root`
	ThriftPort          int      `nimbus.thrift.port`
	StormLocalHostname  string   `storm.local.hostname,omitempty`
	NumbusSeeds         []string `nimbus.seeds,omitempty,flow`
	NimbusHost          string   `nimbus.host,omitempty`
	UiPort              int      `ui.port,omitempty`
	DrpcPort            int      `drpc.port,omitempty`
	
	JavaSecurityAuthLoginConfig string   `java.security.auth.login.config,omitempty`
	//SupervisorChildOpts         string   `supervisor.childopts,omitempty`
	WorkerChildOpts             string   `worker.childopts,omitempty`
	
	Others              map[string]interface{} `yaml:",inline"`
}

var nimbusHosts []string

func main() {
	if len(os.Args) != 2 {
		log.Fatalln("Usage: " + os.Args[0] + " [config/storm.yml]")
	}
	filename, _ := filepath.Abs(os.Args[1])
	log.Println("Reading storm.yml file at: " + filename)
	yamlStr, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	log.Println("Saving a backup copy in: " + filename + "-orig")
	_ = ioutil.WriteFile(filename+"-orig", yamlStr, 0644)

	log.Println("Parsing YAML...")
	//m := make(map[interface{}]interface{})
	//err = yaml.Unmarshal([]byte(yamlStr), &m)
	c := &config{}
	err = yaml.Unmarshal([]byte(yamlStr), c)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	
	{
		numWorkers, err := strconv.Atoi(os.Getenv("NUM_WORKERS_PER_SUPERVISOR"))
		log.Println("Checking for NUM_WORKERS_PER_SUPERVISOR:", numWorkers)
		if err == nil {
			slotPorts := []int{}
			for i := 0; i < numWorkers; i++ {
				slotPorts = append(slotPorts, 6700 + i)
			}
			
			if (len(slotPorts) > 0) {
				c.SupervisorSlotPorts = slotPorts
				delete(c.Others, "supervisor.slots.ports")
				log.Println("c.SupervisorSlotPorts =", c.SupervisorSlotPorts)
			}
		}
	}

	{
		//delete(m, "storm.zookeeper.servers")

		zookeeperServers := []string{}

		serverNumber := 1
		stop := false
		for serverNumber < 15 && !stop {
			hostEnvVar := "ZK_SERVER_" + strconv.Itoa(serverNumber) + "_SERVICE_HOST"
			log.Println("Checking for Zookeeper host in environment variable: " + hostEnvVar)
			hostEnv := os.Getenv(hostEnvVar)
			if len(hostEnv) == 0 {
				log.Println("Not Found. Stopping here")
				stop = true
			} else {
				zookeeperServers = append(zookeeperServers, hostEnv)
				log.Printf("Found server: %v\n", hostEnv)
			}
			serverNumber += 1
		}

		if len(zookeeperServers) > 0 {
			// m["storm.zookeeper.servers"] = zookeeperServers
			c.ZkServers = zookeeperServers
			delete(c.Others, "storm.zookeeper.servers")
		} else {
			log.Println("WARNING - No Zookeeper servers found. Keeping as the default localhost")
		}
	}

	{
		log.Println("Checking for zookeeper root in env APACHE_STORM_ZK_ROOT")
		zkRoot := os.Getenv("APACHE_STORM_ZK_ROOT")
		if len(zkRoot) == 0 {
			log.Println("No zookeeper root found, keeping the default")
		} else {
			log.Println("Using zookeeper root " + zkRoot)
			//m["storm.zookeeper.root"] = zkRoot
			c.ZkRoot = zkRoot
			delete(c.Others, "storm.zookeeper.root")
		}
	}

	{
		log.Println("Checking for nimbus server in env APACHE_STORM_NIMBUS_SERVICE_PORT")
		thriftPort := os.Getenv("APACHE_STORM_NIMBUS_SERVICE_PORT")
		if len(thriftPort) == 0 {
			log.Println("No nimbus thrift port found, keeping the default")
		} else {
			log.Println("Using nimbus thrift port " + thriftPort)
			port, _ := strconv.Atoi(thriftPort)
			//m["nimbus.thrift.port"] = port
			c.ThriftPort = port
			delete(c.Others, "nimbus.thrift.port")
		}
	}

	{
		log.Println("Checking for nimbus server in env APACHE_STORM_NIMBUS_SERVICE_HOST")
		nimbusServer := os.Getenv("APACHE_STORM_NIMBUS_SERVICE_HOST")
		nimbusHosts = append(nimbusHosts, nimbusServer)
		if len(nimbusServer) == 0 {
			log.Println("No nimbus servers found, keeping the default")
		} else {
			log.Println("Using nimbus server " + nimbusServer)
			
			// if os.Getenv("STORM_CMD") == "nimbus" {
			//         //m["storm.local.hostname"] = nimbusServer
			// 		c.StormLocalHostname = nimbusServer
			// 		delete(c.Others, "storm.local.hostname")
			// } else {
			//         //m["nimbus.seeds"] = []string{nimbusServer}
			// 		c.NumbusSeeds = []string{nimbusServer}
			// 		delete(c.Others, "nimbus.seeds")
			// 		c.NimbusHost = nimbusServer
			// 		delete(c.Others, "nimbus.host")
			// }

				c.StormLocalHostname = nimbusServer
				delete(c.Others, "storm.local.hostname")

				c.NumbusSeeds = []string{nimbusServer}
				delete(c.Others, "nimbus.seeds")
				c.NimbusHost = nimbusServer
				delete(c.Others, "nimbus.host")
		
		}
	}
	
	if os.Getenv("STORM_CMD") == "ui" {
        	//m["ui.port"] = 8080
			c.UiPort = 8080
			delete(c.Others, "ui.port")
	}
	
	if os.Getenv("STORM_CMD") == "drpc" {
        	//m["drpc.port"] = 3772
			c.DrpcPort = 3772
			delete(c.Others, "drpc.port")
	}
	
	{
		krb5Conf := os.Getenv("KRB5_CONF_CONTENT")
		kafkaKeyTab := os.Getenv("KAFKA_CLIENT_KEY_TAB_CONTENT")
		kafkaServiceName := os.Getenv("KAFKA_CLIENT_SERVICE_NAME")
		kafkaPrincipal := os.Getenv("KAFKA_CLIENT_PRINCIPAL")
		if krb5Conf != "" && kafkaKeyTab != "" && kafkaServiceName != "" && kafkaPrincipal != "" {
			delete(c.Others, "java.security.auth.login.config")
			delete(c.Others, "supervisor.childopts")
			delete(c.Others, "worker.childopts")
			
			stormJaasConfPath := "/opt/apache-storm" + "/conf/storm_jaas.conf"
			c.JavaSecurityAuthLoginConfig = stormJaasConfPath
			//c.SupervisorChildOpts = "-Djava.security.auth.login.config=" + stormJaasConfPath
			c.WorkerChildOpts = "-Djava.security.auth.login.config=" + stormJaasConfPath
			
			// modify file "/etc/krb5.conf"
			krb5ConfContent, err := base64.StdEncoding.DecodeString(krb5Conf)
			if err != nil {
				log.Println("decode krb5Conf error:", err)
			}
			err = ioutil.WriteFile("/etc/krb5.conf", krb5ConfContent, 0644)
			if err != nil {
				log.Println("write /etc/krb5.conf error:", err)
			}
			
			// modify file "/etc/security/keytabs/storm.headless.keytab"
			kafakKeytabContent, err := base64.StdEncoding.DecodeString(kafkaKeyTab)
			if err != nil {
				log.Println("decode kafkaKeyTab error:", err)
			}
			keytabFilePath := "/etc/security/keytabs/storm.headless.keytab"
			os.MkdirAll(filepath.Dir(keytabFilePath), 0755)
			err = ioutil.WriteFile(keytabFilePath, kafakKeytabContent, 0644)
			if err != nil {
				log.Println("write /etc/security/keytabs/storm.headless.keytab error:", err)
			}
			
			jaasConf := fmt.Sprintf(`KafkaClient {
   com.sun.security.auth.module.Krb5LoginModule required
   useKeyTab=true
   keyTab="%s"
   storeKey=true
   useTicketCache=false
   serviceName="%s"
   principal="%s";
};
`, keytabFilePath, kafkaServiceName, kafkaPrincipal)
			
			// modify file stormJaasConfPath
			err = ioutil.WriteFile(stormJaasConfPath, []byte(jaasConf), 0644)
			if err != nil {
				log.Println("write", stormJaasConfPath, "error:", err)
			}
		}
	}

	{
		log.Println("Trying to save modified config")
		//d, err := yaml.Marshal(&m)
		d, err := yaml.Marshal(c)
		if err != nil {
			log.Fatalf("error: %v", err)
		}

		// may be a wrong fix for storm ui problem.
		if bytes.Index(d, []byte("nimbus.seeds:")) >= 0 {
			for _, host := range nimbusHosts {
				quotedHost := `"` + host + `"`
				if bytes.Index(d, []byte(quotedHost)) < 0 {
					d = bytes.Replace(d, []byte(host), []byte(quotedHost), -1)
				}
			}
		}

		err = ioutil.WriteFile(filename, d, 0644)
		if err != nil {
			log.Fatalf("error: %v", err)
		}
	}
}

